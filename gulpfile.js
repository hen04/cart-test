'use strict';

const gulp = require('gulp');
const plugins = require('gulp-load-plugins')();

const style = require('gulp-sass');

const clean = require('gulp-clean');

const svgCheerio = require('gulp-cheerio');
const svgReplace = require('gulp-replace');
const svgSprite = require('gulp-svg-sprite');
const svgMin = require('gulp-svgmin');

const pngquant = require('imagemin-pngquant');

const data = require('gulp-data');
const pathJson = require('gulp-path');
const fs = require('fs');

const gcmq = require('gulp-group-css-media-queries');

const runSequence = require('run-sequence'); // последовательная обработка build

const urlAdjuster = require('gulp-css-url-adjuster'); //плагин для подмены адреса у background

const browserSync = require('browser-sync').create();

// Пути для сборки
var path = {
	build: {
		root: 'build/',
		html: 'build/',
		json: 'build/data/',
		js: 'build/js/',
		jslibs: 'build/js/libs/',
		css: 'build/css/',
		csslibs: 'build/css/libs/',
		fonts:  'build/fonts/',
		files:  'build/files/',
		favicon: 'build/',
		maps: '/maps',
		images: 'build/images/',
		svg: './build/images/icons/'
	},
	src: {
		root: 'src/',
		pug: 'src/**/*.pug',
		jsonDir: 'src/data/',
		json: 'src/data/**/*.json',
		scss: 'src/scss/**/*.{scss, css}',
		csslibs: 'src/scss/libs/*.css',
		fonts: 'src/fonts/*.*',
		files: 'src/files/*.*',
		favicon: 'src/favicon.ico',
		images: 'src/images/**/*.{png,jpg,gif,svg}',
		svg: 'src/images/icons/sprite/*.svg',
		spriteStyle: 'scss/_sprite.scss',
		spriteTemplate: 'src/scss/templates/_sprite-template.scss',
		js: 'src/js/**/*.js',
	},
	zip: {
		build: 'build.zip'
	},
	watch: {
		root: './build/'
	}
};

// pug
gulp.task('pug', function(){
	return gulp.src(path.src.pug)
		.pipe(plugins.pug({pretty: true}))
		.on('error', console.log)
		.pipe(gulp.dest(path.build.html))
		.on('end', browserSync.reload);
});


gulp.task('json', function() {
	return gulp.src(path.src.json)
		.pipe(gulp.dest(path.build.json))
		.on('end', browserSync.reload);
});

// style только для разработки, без склеивания
gulp.task('style', function () {
	return gulp.src(path.src.scss)
		.pipe(plugins.sourcemaps.init({loadMaps: true}))
		.pipe(style({outputStyle: 'expanded'}).on('error', style.logError))
		.pipe(plugins.autoprefixer({browsers: ['last 2 versions']}))
		.pipe(gcmq())
		.pipe(plugins.sourcemaps.write(path.build.maps))
		.pipe(gulp.dest(path.build.css))
		.on('end', browserSync.reload);
});


// csslibs
gulp.task('csslibs', function () {
	return gulp.src(path.src.csslibs) // тут пути
		.pipe(gulp.dest(path.build.csslibs));
});

// fonts
gulp.task('fonts', function () {
	return gulp.src(path.src.fonts) // тут пути
		.pipe(gulp.dest(path.build.fonts));
});

// files
gulp.task('files', function () {
	return gulp.src(path.src.files) // тут пути
		.pipe(gulp.dest(path.build.files));
});

// favicon
gulp.task('favicon', function () {
	return gulp.src(path.src.favicon) // тут пути
		.pipe(gulp.dest(path.build.favicon));
});

// js
gulp.task('js', function () {
	return gulp.src(path.src.js)
		.pipe(gulp.dest(path.build.js))
		.on('end', browserSync.reload);
});

// svg-sprite
gulp.task('svg', function() {
	return gulp.src(path.src.svg)
		.pipe(svgMin({
			js2svg: {
				pretty: true
			}
		}))
		.pipe(svgCheerio({
			run: function($) {
				//$('[fill]').removeAttr('fill');
				$('[stroke]').removeAttr('stroke');
				$('[style]').removeAttr('style');
			},
			parseOptions: {xmlMode: true}
		}))
		.pipe(svgReplace('&gt;', '>'))
		.pipe(svgSprite({
			shape: {
				spacing: {
					padding: 1
				}
			},
			mode: {
				css: {
					dest: '.',
					bust: false,
					sprite: 'sprite.svg',
					render: {
						scss: {
							dest: path.src.spriteStyle,
							template: path.src.spriteTemplate
						}
					}
				}
			}
		}))
		.pipe(gulp.dest(path.build.svg));
});


// imagemin
gulp.task('images', function () {
	return gulp.src(path.src.images)
		//.pipe(newer(path.build.images))
		.pipe(plugins.imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()],
			interlaced: true
		}))
		.pipe(gulp.dest(path.build.images))
});


// очистка папки build
gulp.task('clean', function() {
	return gulp.src(path.build.root, {read: false})
		.pipe(clean());
});

// очистка папки css
gulp.task('cleancss', function() {
	return gulp.src(path.build.css, {read: false})
		.pipe(clean());
});

// очистка папки build
gulp.task('cleanzip', function() {
	return gulp.src(path.zip.build, {read: false})
		.pipe(clean());
});

// webserver
gulp.task('webserver', function () {
	return browserSync.init({
		server: {
			baseDir: path.watch.root
		},
		port: "8000",
		open: false
	});
});



// watch
gulp.task('default', ['webserver'], function(){
	gulp.watch(path.src.json, ['json', 'pug']);
	gulp.watch(path.src.pug, ['pug']);
	gulp.watch(path.src.images, ['images']);
	gulp.watch(path.src.js, ['js']);
	gulp.watch(path.src.svg, ['svg', 'style']);
	gulp.watch(path.src.fonts, ['fonts']);
	gulp.watch(path.src.files, ['files']);
	gulp.watch(path.src.scss, ['csslibs', 'style']);
	gulp.watch(path.src.scss, ['style']);
});


// build - для разработки
gulp.task('build', function(cb) {
	runSequence('clean', 'json', 'pug', 'images', 'js', 'fonts', 'files', 'favicon', 'csslibs', 'style', cb);
});

// очистка стилей для разработки
gulp.task('dev', function(cb) {
	runSequence('cleancss', 'csslibs', 'style', cb);
});




