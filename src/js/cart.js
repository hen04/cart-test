"use strict";

function nextStep() {
	const tabElems = document.querySelectorAll('.js-tabs');
	const tabActives = document.querySelectorAll('.cart-tabs__item');
	const tabTargets = document.querySelectorAll('.cart__step-item');

	for (let tabElem of tabElems) {
		const stepOpen = tabElem.dataset.tab;

		tabElem.addEventListener('click', function () {
			for (let tabActive of tabActives) {
				tabActive.classList.remove('active');
			}
			document.getElementById('tab-' + stepOpen).classList.add('active');

			for (let tabTarget of tabTargets) {
				tabTarget.classList.remove('opened');
			}
			document.getElementById('step-' + stepOpen).classList.add('opened');
		});
	}
}

// следующий шаг
nextStep();


function goodsQuantity(target) {
	const itemCount = document.querySelectorAll(target);
	const goodsPrice = 10500;

	return function() {
		for (let itemPlus of itemCount) {
			itemPlus.addEventListener('click', function () {
				const value = itemPlus.dataset.id;
				const getValue = document.getElementById('val-' + value);
				let summ = 0;

				if (target === '.js-increase') {
					getValue.value++;
				} else if (getValue.value > 1) {
					getValue.value--;
				}

				summ = goodsPrice * getValue.value;
				document.getElementById('summ-' + value).textContent = summ;

				recalculationPrice();

			});
		}
	}
}

// изменение количества товаров
const goodsIncrease = goodsQuantity('.js-increase');
const goodsDecrease = goodsQuantity('.js-decrease');
goodsIncrease();
goodsDecrease();


function recalculationPrice() {
	const totalPrices = document.querySelectorAll('.js-total-price');
	const totalPriceDelivery = document.querySelector('.js-total-price-with-delivery');
	const deliveryPrice = 500;
	const pricesList = document.querySelectorAll('.js-item-price');
	let result = 0;

	for (let priceSumm of pricesList) {
		result += +priceSumm.innerText;
	}

	for (let totalPrice of totalPrices) {
		totalPrice.innerHTML = result;
		const value = totalPrice.innerText;
		const valueDelivery = +value + deliveryPrice;
		totalPriceDelivery.innerText = valueDelivery;

		if (+value === 0) {
			totalPriceDelivery.innerText = 0;
		}
	}
}

function recalculationItem() {
	const item = document.querySelectorAll('.cart-list__item');
	const target = document.querySelector('.js-goods-quantity');
	const delivery = document.querySelector('.js-price-delivery');

	target.innerText = item.length;
	if (item.length === 0) {
		delivery.innerText = 0;
	}
}


function del() {
	const delItems = document.querySelectorAll('.js-del');
	const totalPrise = document.querySelector('.js-total-price');
	const emptyCart = document.querySelector('.cart-list-empty');

	for (let delItem of delItems) {

		delItem.addEventListener('click', function () {
			const value = delItem.dataset.del;
			document.getElementById('item-' + value).remove();

			recalculationPrice();
			recalculationItem();
			promoCode();

			if (+totalPrise.innerText === 0) {
				emptyCart.classList.remove('hidden');
			}

		});
	}
}

// удаление товаров
del();


function promoCode() {
	const totalPrices = document.querySelector('.js-total-price');
	const totalPriceDelivery = document.querySelector('.js-total-price-with-delivery');
	const promoInput = document.querySelector('.js-promo-code');
	const getValue = promoInput.value;

	if (getValue === 'promo_ok') {
		const value = +totalPrices.innerText;
		const tallage = value / 100 * 10;
		const summ = value - tallage;
		totalPriceDelivery.innerText = summ;
	}
}

const promoInput = document.querySelector('.js-promo-code');
promoInput.addEventListener('keyup', function () {
	promoCode();
});

